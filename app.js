const express = require("express");
const app = express();
const port = 8000;
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port, () => {
  console.log(`Projeto executando na porta ${port}`);
});

//CLIENTES
app.get("/clientes", (req, res) => {
  let obj = req.query;
  res.send(`Nome: ${obj.nome}`);
});

app.post("/clientes", (req, res) => {
  let dados = req.body;
  let headers_ = req.headers["access"];
  let accessPassword = "589";

  let ret = "";
  if (headers_ != accessPassword)
  {
    ret = "Não autorizado"
    res.send(`{message: ${ret}}`)
  }
  else
  {
    ret = `Nome: ${dados.nome}`;
    res.send(`{message: ${ret}}`);
  }
});

//FUNCIONÁRIOS
app.get("/funcionarios", (req, res) => {
  let obj = req.query;
  res.send(`Nome: ${obj.nome}`);
});

app.delete("/funcionarios", (req, res) => {
  let headers_ = req.headers["access"];
  let accessPassword = "589";

  let ret = "";
  if (headers_ != accessPassword)
  {
    ret = "Não autorizado"
    res.send(`{message: ${ret}}`)
  }
  else
  {
    ret = `Dados excluídos com sucesso`;
    res.send(`{message: ${ret}}`);
  }
});

app.put("/funcionarios", (req, res) => {
  let dados = req.body;
  let headers_ = req.headers["access"];
  let accessPassword = "589";

  let ret = "";
  if (headers_ != accessPassword)
  {
    ret = "Não autorizado"
    res.send(`{message: ${ret}}`)
  }
  else
  {
    ret = `Dados alterados com sucesso. `;
    ret += `nome: ${dados.nome}`;
    res.send(`{message: ${ret}}`);
  }
});